package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Controller
public class EmployeeController {
	
	@Autowired
	EmployeeRepository repository;
	
	@RequestMapping("/index")
    public ModelAndView myindex() {
        return new ModelAndView("welcome");
    }
	
	
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ModelAndView showForm() {
    	return new ModelAndView("employeeHome", "employee", new Employee());
    }
 
    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("employee")Employee employee, 
      BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        repository.insertEmployee(employee.getId(), employee.getName(), employee.getAge(), employee.getSalary(), employee.getDept());
        model.addAttribute("id", employee.getId());
        model.addAttribute("name", employee.getName());
        model.addAttribute("age", employee.getAge());
        model.addAttribute("salary", employee.getSalary());
        model.addAttribute("dept", employee.getDept());
        return "employeeView";
    }
    
    @RequestMapping(value="/showEmployees", method = RequestMethod.GET)
    public ModelAndView getAllEmployees(){
    	
    	 ObjectMapper mapper = new ObjectMapper(); 
    	
    	ModelAndView mav = new ModelAndView();
    	try {
			mav.addObject("emplist", mapper.writeValueAsString(repository.getAllEmployees()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	mav.setViewName("employeeAll");
    	return mav;
    	
    }
}
