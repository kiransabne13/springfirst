package com.example.demo.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Employee;



@Repository
public class EmployeeRepository {
	
	@Autowired
	JdbcTemplate template;

	//get All Employees;
	public List<Employee> getAllEmployees(){
		List<Employee> empList = template.query("select * from employee",(result, rowNum)-> new Employee(result.getInt("id"), result.getString("name"), result.getInt("age"), result.getFloat("salary"), result.getString("dept")));
		System.out.println("employeeList---->"+ empList);
		return empList;
	}
	
	//get by id
	public Employee getById(int id) {
		
		String query = "select * from employee where id = ? limit 1";
		Employee employee = template.queryForObject(query, new Object[] {id}, new BeanPropertyRowMapper<>(Employee.class));
		return employee;
	}
	
	//insert in employee;
	
	public int insertEmployee(int id, String name, int age, float salary, String dept) {
		String sql = "insert into employee (id, name, age, salary, dept) values (?, ?, ?, ?, ?)";
		return template.update(sql, id, name, age, salary, dept);
	}
	
	
	//delete Employee
	public int deleteEmployee(int id) {
		String sql = "delete from employee where id = ?";
		return template.update(sql, id);
		
	}
	
	//update Employee;
	public int updateEmployee(int id, String name, String dept) {
		String sql = "update employee set name = "+name+" ,  dept = "+dept+" where id = ?";
		return template.update(sql, id);
		
	}
}
