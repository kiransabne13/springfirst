<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>
<body>

  
<h2>${emplist}</h2>

<table id="table_id" class="display" cellspacing="0" width="100%" style="overflow-x:auto">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Age</th>
            <th>Salary</th>
            <th>Dept</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>emplist</td>
            
        </tr>
        <tr>
            <td>Row 2 Data 1</td>
            
        </tr>
    </tbody>
</table>




<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" th:inline="javascript">

  var empList = ${emplist}; 
  

$(document).ready(function () {
	
	  console.log(empList);	
	  
	 
	 $('#table_id').DataTable({
		    data: empList,
		    columns: [
		        { data: 'id' },
		        { data: 'name' },
		        { data: 'age' },
		        { data: 'salary' },
		        { data: 'dept' }
		    ]
       
    }) 
});
</script>
</body>
</html>